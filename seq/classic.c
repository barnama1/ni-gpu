#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define GET(M, i, j, stride) (M + (i*stride) + j)

int N;
float* A;
float* B;
float* C;

int PRINT = 0;


void multiply (float* A, float* B, float* C, int n)
{
   int i, j, k;
   float s;

   for (i = 0; i < n; ++i)
      for (j = 0; j < n; ++j) {
         s = 0;
         for (k = 0; k < n; ++k)
            s += *GET(A, i, k, n) * *GET(B, j, k, n);
         *GET(C, i, j, n) = s;
      }
}


int main (int argc, char** argv)
{
   int i, j;

   if (scanf(" %d", &N) != 1)
      return 1;

   if (argc > 1)
      PRINT = atoi(argv[1]);

   A = (float*)calloc(N * N, sizeof(float));
   B = (float*)calloc(N * N, sizeof(float));
   C = (float*)calloc(N * N, sizeof(float));

   for (i = 0; i < N; ++i)
      for (j = 0; j < N; ++j)
         if (scanf(" %f", GET(A, i, j, N)) != 1)
            return 2;

   for (i = 0; i < N; ++i)
      for (j = 0; j < N; ++j)
         if (scanf(" %f", GET(B, j, i, N)) != 1) // transposed
            return 3;

   clock_t begin = clock();
   multiply(A, B, C, N);
   clock_t end = clock();

   if (PRINT)
      for (i = 0; i < N*N; ++i)
         printf("%.02f ", C[i]);

   printf("%.02lf\n", (double)(end - begin) / CLOCKS_PER_SEC);

   free(A);
   free(B);
   free(C);
   return 0;
}