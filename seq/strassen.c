#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define GET(M, i, j, stride) (M + (i*stride) + j)

float* BUFFER = NULL;
int WR;
int THRESHOLD = 1;

int N;
float* A;
float* B;
float* C;

int PRINT = 0;


float* alloc (int n)
{
   WR += n;
   return BUFFER + WR - n;
}


void add (float* A, float* B, float* C, int strideAB, int strideC, int n)
{
   int i, j;

   for (i = 0; i < n; ++i)
      for (j = 0; j < n; ++j)
         *GET(C, i, j, strideC) = *GET(A, i, j, strideAB) + *GET(B, i, j, strideAB);
}


void sub (float* A, float* B, float* C, int strideAB, int strideC, int n)
{
   int i, j;

   for (i = 0; i < n; ++i)
      for (j = 0; j < n; ++j)
         *GET(C, i, j, strideC) = *GET(A, i, j, strideAB) - *GET(B, i, j, strideAB);
}


void multiply_classic (float* A, float* B, float* C, int strideA, int strideB, int strideC)
{
   int i, j, k, n = strideC;

   for (i = 0; i < n; ++i)
      for (j = 0; j < n; ++j)
         for (k = 0; k < n; ++k)
            *GET(C, i, j, strideC) += *GET(A, i, k, strideA) * *GET(B, j, k, strideB);
}


void multiply_strassen (float* A, float* B, float* C, int strideA, int strideB, int strideC)
{
   int n = strideC;
   int s = n/2;
   int S = s * s;

   if (n <= THRESHOLD) {
      multiply_classic(A, B, C, strideA, strideB, strideC);
      return;
   }

   float* M1 = alloc(S);
   float* M2 = alloc(S);
   float* M3 = alloc(S);
   float* M4 = alloc(S);
   float* M5 = alloc(S);
   float* M6 = alloc(S);
   float* M7 = alloc(S);

   float* A11 = GET(A, 0, 0, strideA);
   float* A12 = GET(A, 0, s, strideA);
   float* A21 = GET(A, s, 0, strideA);
   float* A22 = GET(A, s, s, strideA);

   float* B11 = GET(B, 0, 0, strideB);
   float* B12 = GET(B, s, 0, strideB);
   float* B21 = GET(B, 0, s, strideB);
   float* B22 = GET(B, s, s, strideB);

   float* C11 = GET(C, 0, 0, strideC);
   float* C12 = GET(C, 0, s, strideC);
   float* C21 = GET(C, s, 0, strideC);
   float* C22 = GET(C, s, s, strideC);

   float* tmp1 = alloc(S);
   float* tmp2 = alloc(S);

   sub(B12, B22, tmp1, strideB, s, s);
   multiply_strassen(A11, tmp1, M1, strideA, s, s);

   add(A11, A12, tmp1, strideA, s, s);
   multiply_strassen(tmp1, B22, M2, s, strideB, s);

   add(A21, A22, tmp1, strideA, s, s);
   multiply_strassen(tmp1, B11, M3, s, strideB, s);

   sub(B21, B11, tmp1, strideB, s, s);
   multiply_strassen(A22, tmp1, M4, strideA, s, s);

   add(A11, A22, tmp1, strideA, s, s);
   add(B11, B22, tmp2, strideB, s, s);
   multiply_strassen(tmp1, tmp2, M5, s, s, s);

   sub(A12, A22, tmp1, strideA, s, s);
   add(B21, B22, tmp2, strideB, s, s);
   multiply_strassen(tmp1, tmp2, M6, s, s, s);

   sub(A11, A21, tmp1, strideA, s, s);
   add(B11, B12, tmp2, strideB, s, s);
   multiply_strassen(tmp1, tmp2, M7, s, s, s);

   add(M5, M6, tmp1, s, s, s);
   sub(M4, M2, tmp2, s, s, s);
   add(tmp1, tmp2, C11, s, strideC, s);

   add(M1, M2, C12, s, strideC, s);
   add(M3, M4, C21, s, strideC, s);

   sub(M1, M7, tmp1, s, s, s);
   sub(M5, M3, tmp2, s, s, s);
   add(tmp1, tmp2, C22, s, strideC, s);
}


int main (int argc, char** argv)
{
   int i, j;

   if (scanf(" %d", &N) != 1)
      return 1;

   if (argc > 1)
      THRESHOLD = atoi(argv[1]);
   else
      THRESHOLD = N/8;

   if (argc > 2)
      PRINT = atoi(argv[2]);

   if ((BUFFER = (float*)calloc(20*N*N, sizeof(float))) == NULL)
      return 2;

   A = alloc(N * N);
   B = alloc(N * N);
   C = alloc(N * N);

   for (i = 0; i < N; ++i)
      for (j = 0; j < N; ++j)
         if (scanf(" %f", GET(A, i, j, N)) != 1)
            return 3;

   for (i = 0; i < N; ++i)
      for (j = 0; j < N; ++j)
         if (scanf(" %f", GET(B, j, i, N)) != 1) // transposed
            return 4;

   clock_t begin = clock();
   multiply_strassen(A, B, C, N, N, N);
   clock_t end = clock();

   if (PRINT)
      for (i = 0; i < N*N; ++i)
         printf("%.02f ", C[i]);

   printf("%.02lf\n", (double)(end - begin) / CLOCKS_PER_SEC);

   free(BUFFER);
   return 0;
}