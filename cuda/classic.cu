#include <stdio.h>
#include <stdlib.h>
#include <cuda_runtime.h>
#include "common.h"

#define GET(M, i, j, stride) (M + (i*stride) + j)

using namespace std;

int BLOCK_DIM = 32;
int PRINT = 0;


__global__ void mult_kernel (float* A, float* B, float* C, int N)
{
   int row = blockIdx.y*blockDim.y+threadIdx.y;
   int col = blockIdx.x*blockDim.x+threadIdx.x;
   float s = 0;

   if (row < N && col < N) {
      for (int i = 0; i < N; i++)
         s += A[row*N + i] * B[i*N + col];
      C[row*N + col] = s;
   }
}


void mult (float *A, float *B, float *C, int N)
{
   dim3 threads_per_block(1, 1);
   dim3 blocks_per_grid(1, 1);

   threads_per_block.x = BLOCK_DIM;
   threads_per_block.y = BLOCK_DIM;
   blocks_per_grid.x = ceil(double(N)/double(threads_per_block.x));
   blocks_per_grid.y = ceil(double(N)/double(threads_per_block.y));

   mult_kernel<<<blocks_per_grid,threads_per_block>>>(A, B, C, N);
}


int main (int argc, char** argv)
{
   cudaEvent_t start;
   cudaEvent_t stop;
   cudaEventCreate(&start);
   cudaEventCreate(&stop);

   int N;
   float time = 0;

   if (argc > 1)
      BLOCK_DIM = atoi(argv[1]);
   if (argc > 2)
      PRINT = atoi(argv[2]);
   if (scanf(" %d", &N) != 1)
      return 1;

   int SIZE = N*N;
   float* host_A = new float[SIZE];
   float* host_B = new float[SIZE];
   float* host_C = new float[SIZE];

   for (int i = 0; i < N; ++i)
      for (int j = 0; j < N; ++j)
         if (scanf(" %f", GET(host_A, i, j, N)) != 1)
            return 2;
   for (int i = 0; i < N; ++i)
      for ( int j = 0; j < N; ++j)
         if (scanf(" %f", GET(host_B, i, j, N)) != 1)
            return 3;

   cudaEventRecord(start,0);

   dev_array<float> dev_A(SIZE);
   dev_array<float> dev_B(SIZE);
   dev_array<float> dev_C(SIZE);

   dev_A.set(host_A, 0, SIZE);
   dev_B.set(host_B, 0, SIZE);

   mult(dev_A.getData(), dev_B.getData(), dev_C.getData(), N);
   dev_C.get(host_C, 0, SIZE);

   cudaDeviceSynchronize();
   cudaEventRecord(stop,0);
   cudaEventSynchronize(stop);
   cudaEventElapsedTime(&time, start, stop);

   if (PRINT)
      for (int i = 0; i < SIZE; ++i)
         printf("%.02f ", host_C[i]);

   printf("%.02f\n", time); // ms

   cudaEventDestroy(start);
   cudaEventDestroy(stop);

   delete [] host_A;
   delete [] host_B;
   delete [] host_C;
   return 0;
}