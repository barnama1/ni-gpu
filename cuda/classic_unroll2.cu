#include <stdio.h>
#include <stdlib.h>
#include <cuda_runtime.h>
#include "common.h"

#define GET(M, i, j, stride) (M + (i*stride) + j)

using namespace std;

int BLOCK_DIM = 32;
int PRINT = 0;


__global__ void mult_kernel (float* A, float* B, float* C, int N)
{
   float tmp1;
   float tmp2;
   float tmp3;
   float tmp4;

   float s1 = 0;
   float s2 = 0;
   float s3 = 0;
   float s4 = 0;

   int col = blockIdx.x*blockDim.x+threadIdx.x;
   int col1 = 2*col;
   int col2 = 2*col + 1;

   int row = blockIdx.y*blockDim.y+threadIdx.y;
   int row1 = 2*row;
   int row2 = 2*row + 1;

   if (row2 < N && col2 < N) {
      for (int i = 0; i < N; i++) {
         tmp1 = B[i*N + col1];
         tmp2 = B[i*N + col2];
         tmp3 = A[row1*N + i];
         tmp4 = A[row2*N + i];
         s1 += tmp3 * tmp1;
         s2 += tmp4 * tmp1;
         s3 += tmp3 * tmp2;
         s4 += tmp4 * tmp2;
      }
      C[row1*N + col1] = s1;
      C[row2*N + col1] = s2;
      C[row1*N + col2] = s3;
      C[row2*N + col2] = s4;
   }
}


void mult (float *A, float *B, float *C, int N)
{
   dim3 threads_per_block(1, 1);
   dim3 blocks_per_grid(1, 1);

   threads_per_block.x = BLOCK_DIM;
   threads_per_block.y = BLOCK_DIM;
   blocks_per_grid.x = ceil((double(N)/double(threads_per_block.x))/2);
   blocks_per_grid.y = ceil((double(N)/double(threads_per_block.y))/2);

   mult_kernel<<<blocks_per_grid,threads_per_block>>>(A, B, C, N);
}


int main (int argc, char** argv)
{
   cudaEvent_t start;
   cudaEvent_t stop;
   cudaEventCreate(&start);
   cudaEventCreate(&stop);

   int N;
   float time = 0;

   if (argc > 1)
      BLOCK_DIM = atoi(argv[1]);
   if (argc > 2)
      PRINT = atoi(argv[2]);
   if (scanf(" %d", &N) != 1)
      return 1;

   int SIZE = N*N;
   float* host_A = new float[SIZE];
   float* host_B = new float[SIZE];
   float* host_C = new float[SIZE];

   for (int i = 0; i < N; ++i)
      for (int j = 0; j < N; ++j)
         if (scanf(" %f", GET(host_A, i, j, N)) != 1)
            return 2;
   for (int i = 0; i < N; ++i)
      for ( int j = 0; j < N; ++j)
         if (scanf(" %f", GET(host_B, i, j, N)) != 1)
            return 3;

   cudaEventRecord(start,0);

   dev_array<float> dev_A(SIZE);
   dev_array<float> dev_B(SIZE);
   dev_array<float> dev_C(SIZE);

   dev_A.set(host_A, 0, SIZE);
   dev_B.set(host_B, 0, SIZE);

   mult(dev_A.getData(), dev_B.getData(), dev_C.getData(), N);
   dev_C.get(host_C, 0, SIZE);

   cudaDeviceSynchronize();
   cudaEventRecord(stop,0);
   cudaEventSynchronize(stop);
   cudaEventElapsedTime(&time, start, stop);

   if (PRINT)
      for (int i = 0; i < SIZE; ++i)
         printf("%.02f ", host_C[i]);

   printf("%.02f\n", time); // ms

   cudaEventDestroy(start);
   cudaEventDestroy(stop);

   delete [] host_A;
   delete [] host_B;
   delete [] host_C;
   return 0;
}