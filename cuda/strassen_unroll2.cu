#include <stdio.h>
#include <stdlib.h>
#include <cuda_runtime.h>
#include "common.h"

#define GET(M, i, j, stride) (M + (i*stride) + j)

using namespace std;

int BLOCK_DIM = 32;
int PRINT = 0;


__global__ void mult_kernel (float* A, float* B, float* C, int N)
{
   float tmp1;
   float tmp2;
   float tmp3;
   float tmp4;

   float s1 = 0;
   float s2 = 0;
   float s3 = 0;
   float s4 = 0;

   int col = blockIdx.x*blockDim.x+threadIdx.x;
   int col1 = 2*col;
   int col2 = 2*col + 1;

   int row = blockIdx.y*blockDim.y+threadIdx.y;
   int row1 = 2*row;
   int row2 = 2*row + 1;

   if (row2 < N && col2 < N) {
      for (int i = 0; i < N; i++) {
         tmp1 = B[i*N + col1];
         tmp2 = B[i*N + col2];
         tmp3 = A[row1*N + i];
         tmp4 = A[row2*N + i];
         s1 += tmp3 * tmp1;
         s2 += tmp4 * tmp1;
         s3 += tmp3 * tmp2;
         s4 += tmp4 * tmp2;
      }
      C[row1*N + col1] = s1;
      C[row2*N + col1] = s2;
      C[row1*N + col2] = s3;
      C[row2*N + col2] = s4;
   }
}


void mult (float *A, float *B, float *C, int N)
{
   dim3 threads_per_block(1, 1);
   dim3 blocks_per_grid(1, 1);

   threads_per_block.x = BLOCK_DIM;
   threads_per_block.y = BLOCK_DIM;
   blocks_per_grid.x = ceil((double(N)/double(threads_per_block.x))/2);
   blocks_per_grid.y = ceil((double(N)/double(threads_per_block.y))/2);

   mult_kernel<<<blocks_per_grid,threads_per_block>>>(A, B, C, N);
}


__global__ void add_kernel (float* A, float* B, float* C, int N)
{
   int row = blockIdx.y*blockDim.y+threadIdx.y;
   int col = blockIdx.x*blockDim.x+threadIdx.x;

   if (row < N && col < N)
      C[row*N + col] = A[row*N + col] + B[row*N + col];
}


void add (float* A, float* B, float* C, int N)
{
   dim3 threads_per_block(1, N);
   dim3 blocks_per_grid(1, 1);

   threads_per_block.x = BLOCK_DIM;
   threads_per_block.y = BLOCK_DIM;
   blocks_per_grid.x = ceil(double(N)/double(threads_per_block.x));
   blocks_per_grid.y = ceil(double(N)/double(threads_per_block.y));

   add_kernel<<<blocks_per_grid,threads_per_block>>>(A, B, C, N);
}


__global__ void sub_kernel (float* A, float* B, float* C, int N)
{
   int row = blockIdx.y*blockDim.y+threadIdx.y;
   int col = blockIdx.x*blockDim.x+threadIdx.x;

   if (row < N && col < N)
      C[row*N + col] = A[row*N + col] - B[row*N + col];
}


void sub (float* A, float* B, float* C, int N)
{
   dim3 threads_per_block(1, 1);
   dim3 blocks_per_grid(1, 1);

   threads_per_block.x = BLOCK_DIM;
   threads_per_block.y = BLOCK_DIM;
   blocks_per_grid.x = ceil(double(N)/double(threads_per_block.x));
   blocks_per_grid.y = ceil(double(N)/double(threads_per_block.y));

   sub_kernel<<<blocks_per_grid,threads_per_block>>>(A, B, C, N);
}


void strassen (float *A, float *B, float *C, int N)
{
   int n = N/2;
   int size = n*n;

   dev_array<float> dev_A11(size);
   dev_array<float> dev_A12(size);
   dev_array<float> dev_A21(size);
   dev_array<float> dev_A22(size);

   dev_array<float> dev_B11(size);
   dev_array<float> dev_B12(size);
   dev_array<float> dev_B21(size);
   dev_array<float> dev_B22(size);

   dev_array<float> dev_C11(size);
   dev_array<float> dev_C12(size);
   dev_array<float> dev_C21(size);
   dev_array<float> dev_C22(size);

   dev_array<float> dev_M1(size);
   dev_array<float> dev_M2(size);
   dev_array<float> dev_M3(size);
   dev_array<float> dev_M4(size);
   dev_array<float> dev_M5(size);
   dev_array<float> dev_M6(size);
   dev_array<float> dev_M7(size);

   dev_array<float> dev_T1(size);
   dev_array<float> dev_T2(size);

   float* A11 = GET(A, 0, 0, N);
   float* A12 = GET(A, 0, n, N);
   float* A21 = GET(A, n, 0, N);
   float* A22 = GET(A, n, n, N);

   float* B11 = GET(B, 0, 0, N);
   float* B12 = GET(B, 0, n, N);
   float* B21 = GET(B, n, 0, N);
   float* B22 = GET(B, n, n, N);

   float* C11 = GET(C, 0, 0, N);
   float* C12 = GET(C, 0, n, N);
   float* C21 = GET(C, n, 0, N);
   float* C22 = GET(C, n, n, N);

   host_to_device(A11, &dev_A11, N, n);
   host_to_device(A12, &dev_A12, N, n);
   host_to_device(A21, &dev_A21, N, n);
   host_to_device(A22, &dev_A22, N, n);

   host_to_device(B11, &dev_B11, N, n);
   host_to_device(B12, &dev_B12, N, n);
   host_to_device(B21, &dev_B21, N, n);
   host_to_device(B22, &dev_B22, N, n);

   sub(dev_B12.getData(), dev_B22.getData(), dev_T1.getData(), n);
   mult(dev_A11.getData(), dev_T1.getData(), dev_M1.getData(), n);

   add(dev_A11.getData(), dev_A12.getData(), dev_T1.getData(), n);
   mult(dev_T1.getData(), dev_B22.getData(), dev_M2.getData(), n);

   add(dev_A21.getData(), dev_A22.getData(), dev_T1.getData(), n);
   mult(dev_T1.getData(), dev_B11.getData(), dev_M3.getData(), n);

   sub(dev_B21.getData(), dev_B11.getData(), dev_T1.getData(), n);
   mult(dev_A22.getData(), dev_T1.getData(), dev_M4.getData(), n);

   add(dev_A11.getData(), dev_A22.getData(), dev_T1.getData(), n);
   add(dev_B11.getData(), dev_B22.getData(), dev_T2.getData(), n);
   mult(dev_T1.getData(), dev_T2.getData(), dev_M5.getData(), n);

   sub(dev_A12.getData(), dev_A22.getData(), dev_T1.getData(), n);
   add(dev_B21.getData(), dev_B22.getData(), dev_T2.getData(), n);
   mult(dev_T1.getData(), dev_T2.getData(), dev_M6.getData(), n);

   sub(dev_A11.getData(), dev_A21.getData(), dev_T1.getData(), n);
   add(dev_B11.getData(), dev_B12.getData(), dev_T2.getData(), n);
   mult(dev_T1.getData(), dev_T2.getData(), dev_M7.getData(), n);

   add(dev_M5.getData(), dev_M6.getData(), dev_T1.getData(), n);
   sub(dev_M4.getData(), dev_M2.getData(), dev_T2.getData(), n);
   add(dev_T1.getData(), dev_T2.getData(), dev_C11.getData(), n);
   device_to_host(&dev_C11, C11, N, n);

   add(dev_M1.getData(), dev_M2.getData(), dev_C12.getData(), n);
   device_to_host(&dev_C12, C12, N, n);

   add(dev_M3.getData(), dev_M4.getData(), dev_C21.getData(), n);
   device_to_host(&dev_C21, C21, N, n);

   sub(dev_M1.getData(), dev_M7.getData(), dev_T1.getData(), n);
   sub(dev_M5.getData(), dev_M3.getData(), dev_T2.getData(), n);
   add(dev_T1.getData(), dev_T2.getData(), dev_C22.getData(), n);
   device_to_host(&dev_C22, C22, N, n);
}


int main (int argc, char** argv)
{
   cudaEvent_t start;
   cudaEvent_t stop;
   cudaEventCreate(&start);
   cudaEventCreate(&stop);

   int N;
   float time = 0;

   if (argc > 1)
      BLOCK_DIM = atoi(argv[1]);
   if (argc > 1)
      PRINT = atoi(argv[2]);
   if (scanf(" %d", &N) != 1)
      return 1;

   int SIZE = N*N;
   float* host_A = new float[SIZE];
   float* host_B = new float[SIZE];
   float* host_C = new float[SIZE];

   for (int i = 0; i < N; ++i)
      for (int j = 0; j < N; ++j)
         if (scanf(" %f", GET(host_A, i, j, N)) != 1)
            return 2;
   for (int i = 0; i < N; ++i)
      for ( int j = 0; j < N; ++j)
         if (scanf(" %f", GET(host_B, i, j, N)) != 1)
            return 3;

   cudaEventRecord(start,0);

   strassen(host_A, host_B, host_C, N);

   cudaDeviceSynchronize();
   cudaEventRecord(stop,0);
   cudaEventSynchronize(stop);
   cudaEventElapsedTime(&time, start, stop);

   if (PRINT)
      for (int i = 0; i < SIZE; ++i)
         printf("%.02f ", host_C[i]);

   printf("%.02f\n", time); // ms

   cudaEventDestroy(start);
   cudaEventDestroy(stop);

   delete [] host_A;
   delete [] host_B;
   delete [] host_C;
   return 0;
}