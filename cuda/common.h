#include <stdexcept>
#include <cuda_runtime.h>


template <class T>
struct dev_array
{
    dev_array(size_t size) {
       allocate(size);
    }

    ~dev_array() {
       free();
    }

    size_t getSize() const {
       return end_ - start_;
    }

    const T* getData() const {
       return start_;
    }

    T* getData() {
       return start_;
    }

    void set(const T* src, int index, size_t size) {
       size_t min = std::min(size, getSize());
       cudaError_t result = cudaMemcpy(start_ + index, src, min * sizeof(T), cudaMemcpyHostToDevice);
       if (result != cudaSuccess) {
          throw std::runtime_error("failed to copy to device memory");
       }
    }

    void get(T* dest, int index, size_t size) {
       size_t min = std::min(size, getSize());
       cudaError_t result = cudaMemcpy(dest, start_ + index, min * sizeof(T), cudaMemcpyDeviceToHost);
       if (result != cudaSuccess) {
          throw std::runtime_error("failed to copy to host memory");
       }
    }

    void allocate(size_t size) {
       cudaError_t result = cudaMalloc((void**)&start_, size * sizeof(T));
       if (result != cudaSuccess) {
          start_ = end_ = 0;
          throw std::runtime_error("failed to allocate device memory");
       }
       end_ = start_ + size;
    }

    void free() {
       if (start_ != 0) {
          cudaFree(start_);
          start_ = end_ = 0;
       }
    }

    T* start_;
    T* end_;
};


void host_to_device (float* src, dev_array<float>* dst, int stride_src, int N)
{
   for (int i = 0; i < N; ++i)
      dst->set(src + i*stride_src, i*N, N);
}


void device_to_host (dev_array<float>* src, float* dst, int stride_dst, int N)
{
   for (int i = 0; i < N; ++i)
      src->get(dst + i*stride_dst, i*N, N);
}
